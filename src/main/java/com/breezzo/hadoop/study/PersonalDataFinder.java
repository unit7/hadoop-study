package com.breezzo.hadoop.study;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by breezzo on 12.07.16.
 */
public class PersonalDataFinder {

    public static class DataMapper extends Mapper<Text, Text, Text, PersonalData> {
        private static final Text outputKey = new Text("breezzo");

        @Override
        protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            System.out.println("mapper: " + key + " => " + value);

            String stringKey = key.toString();
            String stringValue = value.toString();
            PersonalData data = PersonalData.of(stringKey, stringValue);
            context.write(outputKey, data);
        }
    }

    public static class DataReducer extends Reducer<Text, PersonalData, Text, PersonalData> {
        @Override
        protected void reduce(Text key, Iterable<PersonalData> values, Context context) throws IOException, InterruptedException {
            System.out.println("reducer: " + key + " => " + values);

            PersonalData resultInfo = new PersonalData();
            resultInfo.setIdentity(key.toString());

            for (PersonalData dataItem : values) {
                enrich(resultInfo, dataItem);
            }

            context.write(key, resultInfo);
        }

        private void enrich(PersonalData target, PersonalData source) {
            if (canEnrich(source.getFirstName(), target.getFirstName())) {
                target.setFirstName(source.getFirstName());
            }
            if (canEnrich(source.getLastName(), target.getLastName())) {
                target.setLastName(source.getLastName());
            }
            if (canEnrich(source.getAddress(), target.getAddress())) {
                target.setAddress(source.getAddress());
            }
            if (source.getAge() != 0 && target.getAge() == 0) {
                target.setAge(source.getAge());
            }
            if (canEnrich(source.getCompanyName(), target.getCompanyName())) {
                target.setCompanyName(source.getCompanyName());
            }
            if (canEnrich(source.getCompanyAddress(), target.getCompanyAddress())) {
                target.setCompanyAddress(source.getCompanyAddress());
            }
            if (canEnrich(source.getEmail(), target.getEmail())) {
                target.setEmail(source.getEmail());
            }
            if (canEnrich(source.getPhone(), target.getPhone())) {
                target.setPhone(source.getPhone());
            }
        }

        private boolean canEnrich(String source, String target) {
            return (source != null && !source.isEmpty()) && (target == null || target.isEmpty());
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        String[] remainingArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        Job job = Job.getInstance(conf, "personalDataFinder");

        job.setJarByClass(PersonalDataFinder.class);
        job.setMapperClass(DataMapper.class);
        job.setReducerClass(DataReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(PersonalData.class);
        job.setInputFormatClass(KeyValueTextInputFormat.class);

        for (int i = 0; i < remainingArgs.length - 1; ++i) {
            FileInputFormat.addInputPath(job,
                    new Path("hdfs://localhost:9000/user/breezzo/" + remainingArgs[i]));
        }

        FileOutputFormat.setOutputPath(job,
                new Path("hdfs://localhost:9000/user/breezzo/" + remainingArgs[remainingArgs.length - 1]));

        boolean result = job.waitForCompletion(true);
        System.out.println("Job was completed: " + result);
    }

    public static class PersonalData implements Writable {
        private String firstName;
        private String lastName;
        private int age;
        private String address;
        private String email;
        private String phone;
        private String companyName;
        private String companyAddress;
        private String identity;

        public static PersonalData of(String key, String value) {
            PersonalData personalData = new PersonalData();

            switch (key) {
                case "firstName":
                    personalData.setFirstName(value);
                    break;
                case "lastName":
                    personalData.setLastName(value);
                    break;
                case "age":
                    personalData.setAge(Integer.parseInt(value));
                    break;
                case "address":
                    personalData.setAddress(value);
                    break;
                case "email":
                    personalData.setEmail(value);
                    break;
                case "phone":
                    personalData.setPhone(value);
                    break;
                case "companyName":
                    personalData.setCompanyName(value);
                    break;
                case "companyAddress":
                    personalData.setCompanyAddress(value);
                    break;
                default:
                    break;
            }

            return personalData;
        }

        public String getFirstName() {
            return firstName;
        }

        public PersonalData setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public String getLastName() {
            return lastName;
        }

        public PersonalData setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public int getAge() {
            return age;
        }

        public PersonalData setAge(int age) {
            this.age = age;
            return this;
        }

        public String getAddress() {
            return address;
        }

        public PersonalData setAddress(String address) {
            this.address = address;
            return this;
        }

        public String getEmail() {
            return email;
        }

        public PersonalData setEmail(String email) {
            this.email = email;
            return this;
        }

        public String getPhone() {
            return phone;
        }

        public PersonalData setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public String getCompanyName() {
            return companyName;
        }

        public PersonalData setCompanyName(String companyName) {
            this.companyName = companyName;
            return this;
        }

        public String getCompanyAddress() {
            return companyAddress;
        }

        public PersonalData setCompanyAddress(String companyAddress) {
            this.companyAddress = companyAddress;
            return this;
        }

        public String getIdentity() {
            return identity;
        }

        public PersonalData setIdentity(String identity) {
            this.identity = identity;
            return this;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("PersonalData{");
            sb.append("firstName='").append(firstName).append('\'');
            sb.append(", lastName='").append(lastName).append('\'');
            sb.append(", age=").append(age);
            sb.append(", address='").append(address).append('\'');
            sb.append(", email='").append(email).append('\'');
            sb.append(", phone='").append(phone).append('\'');
            sb.append(", companyName='").append(companyName).append('\'');
            sb.append(", companyAddress='").append(companyAddress).append('\'');
            sb.append(", identity='").append(identity).append('\'');
            sb.append('}');
            return sb.toString();
        }

        @Override
        public void write(DataOutput out) throws IOException {
            out.writeUTF(toEmpty(identity));
            out.writeUTF(toEmpty(firstName));
            out.writeUTF(toEmpty(lastName));
            out.writeInt(age);
            out.writeUTF(toEmpty(phone));
            out.writeUTF(toEmpty(email));
            out.writeUTF(toEmpty(address));
            out.writeUTF(toEmpty(companyName));
            out.writeUTF(toEmpty(companyAddress));
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            identity = in.readUTF();
            firstName = in.readUTF();
            lastName = in.readUTF();
            age = in.readInt();
            phone = in.readUTF();
            email = in.readUTF();
            address = in.readUTF();
            companyName = in.readUTF();
            companyAddress = in.readUTF();
        }

        private String toEmpty(String string) {
            return string == null ? "" : string;
        }
    }
}
